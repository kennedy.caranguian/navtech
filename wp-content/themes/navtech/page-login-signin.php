<?php
get_header(); 
/* Template Name: Log-in and Sign-in */
?>
<main>
    <?php while(have_posts()) : the_post(); ?>
        <div class="container login">
        	<div class="col">
        		<div class="text-con">
        			Use it for free. No Credit card required.
        			or subscribe premium services
        		</div>
        	</div>
        	<div class="col">
        		<?php the_content(); ?>
        	</div>
        	
        </div>
    <?php endwhile; // End of the loop. ?>
</main>
<?php
get_footer();