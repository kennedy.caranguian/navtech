<?php
get_header();	
?>

<main>
    <?php while(have_posts()) : the_post(); ?>
        <div class="container default-page">
        	<?php the_content(); ?>
        </div>
    <?php endwhile; // End of the loop. ?>
</main><!-- #primary -->

<?php
get_footer();