<!DOCTYPE HTML>
<html <?php language_attributes(); ?> style="margin-top:0 !important;">
<head>
<title><?php bloginfo('name'); ?> | <?php wp_title(); ?></title>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<header>
	<div class="top">
		<div class="container">
			<a href="<?php echo home_url(); ?>" class="logo">
				Navtech
			</a>
			<p><?php echo get_bloginfo( 'description', 'display' ); ?></p>
		</div>
	</div>
	<div class="bottom">
		<div class="container inner">
			<div class="col">
				<nav>
					<?php echo wp_nav_menu( array(
					    'menu'   => 'Header Menu'
					)); ?>
				</nav>
				<div class="hamburger hamburger-mobile">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="col header-btn">
				<?php echo wp_nav_menu( array(
				    'menu'   => 'Header Button'
				)); ?>
			</div>
		</div>
	</div>
</header>