<?php
/**
 * Category Template: Pillar
*/

get_header(); 

$obj = get_queried_object(); 
$cat_id = get_post_meta($the_query->ID, '_yoast_wpseo_primary_category', true);
$category = get_the_category($cat_id); 
$category_link = get_category_link( $category[0]->term_id );
?>
<main>
    <?php get_template_part( 'template_parts/section-category-hero' ); ?>
    <?php if (function_exists ('adinserter')) echo adinserter (1); ?>
    <div class="container pillar-body">
        <?php 
            $reverse = "";
            if( get_field('sidebar_position',$obj) == 'right' ) {
                $reverse = "reverse";
            }
        ?>
        <div class="flex-con <?php echo $reverse; ?>">
            <div class="left">
                <?php side_menus(); ?>
                <?php if (function_exists ('adinserter')) echo adinserter (2); ?>
            </div>
            <div class="right">
                <div class="category-body">
                    <!-- Pillar [Start] -->
                    <?php echo category_description( $category_id ); ?>
                    <!-- Pillar [End] -->
                </div>
            </div>
        </div>
    </div>
    <?php if(get_field('faq',$obj)): ?>
    <div class="category-bottom">
        <div class="container">
            <?php the_field('faq',$obj); ?>
        </div>   
    </div>
    <?php endif; ?>
</main>
<?php if (function_exists ('adinserter')) echo adinserter (4); ?>
<?php
get_footer();