(function($) {


	function mobileMenuAction(){
		// mobile menu open/animation
		$('header .hamburger-mobile').click(function(){
			$(this).toggleClass('open');
			$(this).parent('.inner').find('nav').toggleClass('open');
		});
	}

	function threePicksCarousel(){
		if ($(window).width() < 992) {

		  	$(".three-picks").each(function(){
			    $(this).owlCarousel({
			      	loop:false,
			        margin:10,
			        nav:true,
			        navText : ["<i class='fas fa-caret-left'></i>","<i class='fas fa-caret-right'></i>"],
			        responsive:{
			            0:{
			                items:1
			            },
			            600:{
			                items:1
			            },
			            1000:{
			                items:1
			            }
			        }
			    });
			});
			$(".types-of").each(function(){
			    $(this).owlCarousel({
			      	loop:false,
			        margin:10,
			        nav:true,
			        navText : ["<i class='fas fa-caret-left'></i>","<i class='fas fa-caret-right'></i>"],
			        responsive:{
			            0:{
			                items:1
			            },
			            600:{
			                items:1
			            },
			            1000:{
			                items:1
			            }
			        }
			    });
			});
		}else{
			$(".three-picks").each(function(){
				$(this).owlCarousel('destroy');
			});
			$(".types-of").each(function(){
				$(this).owlCarousel('destroy');
			});
		}
	}

	function tocAnim(){
		$('.toc .title a').on('click', function(){
			$(this).closest('.toc').find('.toc-body').slideToggle();
			return false;
		})
		$('.toc .toc-body ol li a').on('click', function(){
			$id = jQuery(this).attr('href');
			$('html,body').animate({scrollTop: $(this.hash).offset().top - 50}, 600);
		})
	}

	function matchHeightCon(){
		var query = Modernizr.mq('(min-width: 650px)');
		var query2 = Modernizr.mq('(min-width: 991px)');
		if (query) {
			$('.category-listing .third-level .items .text-con .title').matchHeight();
			$('.category-listing .third-level .items .text-con .excerpt').matchHeight();
		}
		if (query2){
			$('.three-picks-con .three-picks .item .title-con').matchHeight();
			$('.three-picks-con .three-picks .item .desc').matchHeight();
			$('.three-picks-con .three-picks .item .list-con').matchHeight();
			$('.types-of-con .types-of .item .mid .title-con').matchHeight();
			$('.types-of-con .types-of .item .mid .desc').matchHeight();
			$('.types-of-con .types-of .item .mid .list').matchHeight();
			$('.types-of-con .types-of .item .top').matchHeight();
		}
		
	}
	

	$( window ).ready(function() {
		mobileMenuAction();
		threePicksCarousel();
		tocAnim();
		matchHeightCon();
	});

	$( window ).resize(function() {
		threePicksCarousel();
		matchHeightCon();
	});

})( jQuery );