<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'u422810155_navtech' );

/** MySQL database username */
define( 'DB_USER', 'u422810155_navtechuser' );

/** MySQL database password */
define( 'DB_PASSWORD', 'navtech_pass' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R/5inL~81DlsWf1HA]pqnuJ2ZV1i%gcne&f9Q9Gv<7^YKT#w/#j~7=-u[fn;$7D>' );
define( 'SECURE_AUTH_KEY',  'X8USKHzkuzdKSjs3a|k!tmY.5;8Qsjq-wLNFP2foK_]ceZg#{=VOMV}/0EXmY#L8' );
define( 'LOGGED_IN_KEY',    '<;>aljO ]WW (5HA !k[zf!wYSY`r(JcxNJ^4,2wQMe8v{O&t0NX{%,Dn^Pi<d~@' );
define( 'NONCE_KEY',        'mI^B<87!Nsjxn<~IF[@GrXaU?=;vOGnJ@R:s&sp](Gxk.veUr4z4WvN,s,Rr$)Uh' );
define( 'AUTH_SALT',        '~:EvX*lvW~,xm9?JG|h%As^z*,QVEnIDv@!u<!acr{yme#{OusQJH!agw$>/3FSB' );
define( 'SECURE_AUTH_SALT', ';n@ZkY[UVGgt1rU5&[IX&n/)6tk)e<p`t:,g,w$Yaap1wpZdb3c|P+y14iPk>DNR' );
define( 'LOGGED_IN_SALT',   '6pt/X9sTIzqO<h+NP3+H)qcZF^8qm}wDN-rZ8{9?Q0zetQuq#Z*RiK_L)Gq-|X^w' );
define( 'NONCE_SALT',       '7|`[l$s=3SHdKc:T9<Dn1020rwT[R#6Vt8wWED#Jl]riIz@V2/+60*7e_;Y9V6.S' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
